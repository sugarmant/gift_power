const axios = require('../../utils/request')
const date = require('../../utils/date')
Page({
    data: {
        code:'',
        helpInfo:{},
        isLogin:false,
        publicReply:'',
        empowerId:''
    },
    onLoad(e){
        this.loginPop = this.selectComponent('#login-pop')
        this.freshRewardPopup = this.selectComponent("#freshReward-popup")
        this.followPublicPopup = this.selectComponent('#followPublic-popup')
        // this.freshRewardPopup.onHandlePopup()
        this.setData({
            code:e.inviteCode
        })
        this.getHelpInfo()
        if(wx.getStorageSync('token')){
            this.setData({isLogin:true})
        }
    },
    onHelpHim(){
        if(!this.data.isLogin){
            this.loginPop.onHandlePopup()
            return
        }
        wx.showLoading({
            title:'助力中...',
            mask:true
        })
        axios.post('/api/assist/add',{
            code:this.data.code
        }).then(res=>{
            wx.hideLoading()
            if(res.code==1){
                wx.showToast({
                    icon:'none',
                    title:'助力成功!'
                })
                this.freshRewardPopup.onHandlePopup()
                this.getHelpInfo()
            }else{
                wx.showToast({
                    title:res.msg,
                    icon:'none'
                })
            }
        })
    },
    onJoinToo(){
        wx.switchTab({
            url:'/pages/index/index'
        })
    },
    onShowFollowPublicPopup(){
        wx.showLoading({
            title:"领取中...",
            mask:true
        })
        axios.post('/api/game/receive').then(res=>{
            if(res.code==1){
                this.setData({
                    publicReply:res.data.keyword,
                    empowerId:res.data.empower_public_id
                })
                console.log(this.data.publicReply,this.data.empowerId)
                this.followPublicPopup.onHandlePopup()
                wx.hideLoading()
                
            }else{
                wx.showToast({
                    title:'领的人有点多，请稍后再试哦~',
                    icon:'none'
                })
            }
        })
    },
    toDetail(){
        wx.navigateTo({
            url:'/pages/detail/index?id='+this.data.helpInfo.activities.id
        })
    },
    getHelpInfo(){
        axios.post('/api/assist/index',{
            code:this.data.code
        }).then(res=>{
            if(res.code==1){
                res.data.activities.images = res.data.activities.images.split(',')
                console.log(res)
                if(res.data.list.length>0){
                    res.data.list.map(v=>{
                        v.tempTime = date.standard(v.create_time)
                    })
                }
                this.setData({helpInfo:res.data})
            }
        })
    },
    callbackLogin(){
        this.setData({isLogin:true})
        this.onHelpHim()
    },
})
