const axios = require('../../utils/request')

Page({
    data: {
        page:1,
        rewardList:[],
        finished:false
    },
    onLoad(){
        this.getRewardList()
    },
    onReachBottom(){
        this.getRewardList()
    },
    getRewardList(){
        if(this.data.finished) return
        axios.post('/api/gift/index',{
            page:this.data.page
        }).then(res=>{
            if(res.code==1){
                this.data.page+=1
                if(res.data.length>0){
                    res.data.map(v=>{
                        this.data.rewardList.push(v)
                    })
                    this.setData({rewardList:this.data.rewardList})
                    if(res.data.length<10){
                        this.setData({finished:true})    
                    }
                }else{
                    this.setData({finished:true})
                }
            }
        })
    },
    toReceiveGift(e){
        let id = e.currentTarget.dataset.id
        let key = e.currentTarget.dataset.key
        wx.navigateTo({
            url:"/pages/receiveGift/index?giftInfo="+JSON.stringify(this.data.rewardList[key])
        })
    }
})
