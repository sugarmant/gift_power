const axios = require('../../utils/request')

Page({
    data: {
        providerList:[
            
        ],
    },
    onShow(){
        this.getProviderList()
    },
    onSelectProvider(e){
        let key = e.currentTarget.dataset.key
        const pages = getCurrentPages()
        let prePage = pages[pages.length-2]
        if(prePage.route=='pages/publishDetail/index'){    
            prePage.setData({
                providerInfo:this.data.providerList[key]
            })
            wx.navigateBack({
                delta:1,
            })
        }
    },
    tosetProvider_add(e){
        let id = e.currentTarget.dataset.id
        if(id){
            wx.navigateTo({
                url:'/pages/setProvider/add/index?id='+id
            })
            return
        }
        wx.navigateTo({
            url:'/pages/setProvider/add/index'
        })
    },
    getProviderList(){
        axios.post('/api/providers/index').then(res=>{
            if(res.code==1){
                this.setData({providerList:res.data})
            }
        })
    }
})
