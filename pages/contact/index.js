const axios = require('../../utils/request')

Page({
    data: {
        contactList:[
            
        ],
    },
    onShow(){
        this.getContactList()
    },
    onSelectProvider(e){
        let key = e.currentTarget.dataset.key
        const pages = getCurrentPages()
        let prePage = pages[pages.length-2]
        if(prePage.route=='pages/publishDetail/index'){    
            prePage.setData({
                contactInfo:this.data.contactList[key]
            })
            wx.navigateBack({
                delta:1,
            })
        }
    },
    toContact_add(e){
        let id = e.currentTarget.dataset.id
        if(id){
            wx.navigateTo({
                url:'/pages/contact/add/index?id='+id
            })
            return
        }
        wx.navigateTo({
            url:'/pages/contact/add/index'
        })
    },
    getContactList(){
        axios.post('/api/contact/index').then(res=>{
            if(res.code==1){
                this.setData({contactList:res.data})
            }
        })
    }
})
