const axios = require('../../utils/request')

Page({
    data: {
        balance:'****'
    },
    onLoad(){
        this.getBalance()
    },
    onWithdrawal(){
        if(this.data.balance<5){
            wx.showToast({
                title:"满5元才可提现哦~",
                icon:'none'
            })
        }else{
            wx.showToast({
                title:"系统繁忙，请联系客服吧~",
                icon:'none'
            })
        }
    },
    getBalance(){
        axios.post('/api/withdraw/index').then(res=>{
            if(res.code==1){
                this.setData({balance:res.data.money})
            }
        })
    }
})
