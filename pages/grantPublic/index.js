const axios = require('../../utils/request')

Page({
    data: {
        contactList:[
            
        ],
        token:''
    },
    onLoad(){
        this.teachingPopup = this.selectComponent("#teaching-popup")
        this.setData({
            token:wx.getStorageSync('token')
        })
    },
    onShow(){
        this.getContactList()
    },
    onSelectProvider(e){
        let key = e.currentTarget.dataset.key
        const pages = getCurrentPages()
        let prePage = pages[pages.length-2]
        if(prePage.route=='pages/publishDetail/index'){    
            prePage.setData({
                grantedPublic:this.data.contactList[key]
            })
            wx.navigateBack({
                delta:1,
            })
        }
    },
    onShowTeachingPopup(){
        this.teachingPopup.onHandlePopup()
    },
    getContactList(){
        axios.post('/api/empower_public/index').then(res=>{
            if(res.code==1){
                this.setData({contactList:res.data})
            }
        })
    }
})
