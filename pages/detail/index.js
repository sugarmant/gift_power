
const axios = require('../../utils/request')
Page({
    data: {
        id:'',
        detail:{},
        isLogin:false,
        parCode:''
    },
    onLoad(e) {
        this.followPublicPopup = this.selectComponent("#followPublic-popup")
        this.loginPop = this.selectComponent('#login-pop')
        this.moneyBagPopup = this.selectComponent("#moneyBag-popup")
        this.gotRewardPopup = this.selectComponent('#gotReward-popup')
        if(wx.getStorageSync('token')){
            this.data.isLogin = true
        }
        if(e.scene){
            const uri = decodeURIComponent(e.scene)
            let splitedUri = uri.split('&')
            console.log(splitedUri)
            let param = {}
            splitedUri.map(v=>{
                let paramArr = v.split('=')
                param[paramArr[0]] = paramArr[1]

            })
            this.setData({
                id:param.id,
                parCode:param.activeCode
            })
            this.getLoginedDetail()
            return
        }
        
        this.setData({
            id:e.id
        })
        
        this.getLoginedDetail()
    },
    onShareAppMessage(){
        return {
            title:'我在参与领好礼，帮我助个力吧！',
            path:'/pages/help/index?inviteCode='+this.data.detail.task.task_user.code
        }
    },
    onJoinActivity(){
        if(!this.data.isLogin){
            this.loginPop.onHandlePopup()
            return
        }
        wx.showLoading({
            title:'参与抽奖',
            icon:'none'
        })
        axios.post('/api/activities/participant_add',{
            id:this.data.id,
            code:this.data.parCode
        }).then(res=>{
            wx.hideLoading()
            if(res.code==1){
                this.getLoginedDetail()
            }else{
                wx.showToast({
                    title:res.msg,
                    icon:'none'
                })
            }
        })
    },
    onShowActivityEnd(){
        wx.showToast({
            title:'活动已结束了，去看看其他活动吧~',
            icon:'none'
        })
    },
    onReveiveGift(){
        
        wx.showLoading({
            title:'领取中...',
            icon:'none'
        })
        axios.post("/api/gift/receive",{
            id:this.data.detail.received_gift.id,
        }).then(res=>{
            wx.hideLoading()
            if(res.code==1){
                this.getLoginedDetail()
                if(this.data.detail.received_gift.type==1 || this.data.detail.received_gift.type==3){
                    this.gotRewardPopup.onHandlePopup(this.data.detail.received_gift)
                }else{
                    this.moneyBagPopup.onHandlePopup({
                        money:this.data.detail.received_gift.single_num
                    })
                }
            }else{
                wx.showToast({
                    title:res.msg,
                    icon:'none'
                })
            }
        })
    },
    onFollowPublic(){
        this.followPublicPopup.onHandlePopup()
    },
    onCopyKeyword(){
        wx.setClipboardData({
            data:this.data.detail.keyword,
            success:()=>{
                this.followPublicPopup.onClosePopup()
            }
        })
    },
    toAllParticipant(){
        wx.navigateTo({
            url:"/pages/allParticipant/index?id="+this.data.id
        })
    },
    toReceiveGift(e){
        let gift = e.detail
        if(!gift.id) gift = this.data.detail.received_gift
        console.log(gift)
        wx.navigateTo({
            url:'/pages/receiveGift/index?giftInfo='+JSON.stringify(gift)
        })
    },
    toMyReward(){
        wx.navigateTo({
            url:'/pages/myReward/index'
        })
    },
    getLoginedDetail(){
        axios.post('/api/activities/detail',{
            id:this.data.id
        }).then(res=>{
            if(res.code==1){
                res.data.images = res.data.images.split(',')
                this.setData({
                    detail:res.data
                })
            }
        })
    },
    callbackLogin(){
        this.setData({
            isLogin:true
        })
        this.getLoginedDetail()
    },
    
})
