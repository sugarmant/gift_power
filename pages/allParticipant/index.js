
const axios = require('../../utils/request')
Page({
    data: {
        id:'',
        participants:[],
        participant_num:'**'
    },
    onLoad(e) {
        if(e.id){
            this.setData({
                id:e.id
            })
            this.getAllParticipant()
        }
    },
    getAllParticipant(){
        axios.post('/api/participants/activities_list',{
            id:this.data.id
        }).then(res=>{
            if(res.code==1){
                this.setData({
                    participants:res.data.list,
                    participant_num:res.data.participant_num
                })
            }
        })
    }
})
