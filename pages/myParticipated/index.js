const axios = require('../../utils/request')

Page({
    data: {
        page:1,
        rewardList:[],
        finished:false
    },
    onLoad(){
        this.init()
    },
    onReachBottom(){
        this.getPublishedList()
    },
    init(){
        this.getPublishedList()
    },
    getPublishedList(){
        if(this.data.finished) return

        axios.post('/api/participants/index',{
            page:this.data.page
        }).then(res=>{
            if(res.code==1){
                this.data.page+=1
                if(res.data.length>0){
                    res.data.map(v=>{
                        this.data.rewardList.push(v)
                    })
                    this.setData({rewardList:this.data.rewardList})
                    if(res.data.length<10){
                        this.setData({finished:true})
                    }
                }else{
                    this.setData({finished:true})
                }
            }
        })
    },
    toDetail(e){
        let id = e.currentTarget.dataset.id
        console.log(e)
        wx.navigateTo({
            url:'/pages/detail/index?id='+id
        })
    }
})
