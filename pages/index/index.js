const axios = require('../../utils/request')

Page({
    data: {
        isFreshman:true,
        isLogin:false,
        freshmanGifts:[],
        recommendList:[],
        activities:[],
        publicReply:'',
        token:'',
        pages:1,
        finished:false
    },
    onLoad(e) {
        if(e.scene){
            const uri = decodeURIComponent(e.scene)
            let splitedUri = uri.split('&')
            console.log(splitedUri)
            let param = {}
            splitedUri.map(v=>{
                let paramArr = v.split('=')
                param[paramArr[0]] = paramArr[1]

            })
            if(param.activeCode){
                axios.post('/api/game/activation',{
                    code:param.activeCode
                }).then(res=>{
                    if(res.code==1){
                        let userInfo = wx.getStorageSync('userInfo')
                        userInfo.is_receive = 1
                        wx.setStorageSync('userInfo',userInfo)
                        this.setData({
                            isFreshman:false
                        })
                        this.toReceiveGift({detail:res.data})
                    }
                })
            }
        }
        this.rewardListPopup = this.selectComponent('#rewardList-popup')
        this.moneyBagPopup = this.selectComponent("#moneyBag-popup")
        this.loginPop = this.selectComponent('#login-pop')
        this.gotRewardPopup = this.selectComponent('#gotReward-popup')
        this.followPublicPopup = this.selectComponent('#followPublic-popup')
        this.init()                                
    },
    onShow(e){
        let userInfo = wx.getStorageSync('userInfo')
        if(userInfo.is_receive == 1){
            this.setData({
                isFreshman:false
            })
        }
        
        if(wx.getStorageSync('token')){
            this.setData({isLogin:true})
        }else{
            wx.hideTabBar({})
        }
    },
    onPullDownRefresh(){
        this.init()
    },
    onReachBottom(){
        this.getActivities()
    },
    init(){
        axios.post('/api/game/index').then(res=>{
            wx.stopPullDownRefresh()
            if(res.code==1){
                this.setData({freshmanGifts:res.data})
            }
        })
        this.getRecommendList()
        this.getActivities()
    },
    onHandleRewardListPopup(){
        
        this.rewardListPopup.onHandlePopup(this.data.freshmanGifts)
    },
    onReceiveFreshGift(){
        if(!this.data.isLogin){
            this.loginPop.onHandlePopup()
            return
        }
        //——————————————————————
        wx.showLoading({
            title:"领取中...",
            mask:true
        })
        axios.post('/api/game/receive').then(res=>{
            if(res.code==1){
                this.setData({
                    publicReply:res.data.keyword,
                    empowerId:res.data.empower_public_id
                })
                wx.hideLoading()
                if(res.data.type==2){
                    this.moneyBagPopup.onHandlePopup({
                        money:'??.??'
                    })
                }else{
                    this.gotRewardPopup.onHandlePopup(res.data)
                }
            }else{
                wx.showToast({
                    title:'领的人有点多，请稍后再试哦~',
                    icon:'none'
                })
            }
        })
    },
    onShowFollowPublic(){
        this.setData({
            token:wx.getStorageSync('token')
        })
        this.moneyBagPopup.onClose()
        this.followPublicPopup.onHandlePopup()
    },
    onCopyKeyword(){
        wx.setClipboardData({
            data:this.data.publicReply,
            success:()=>{
                this.followPublicPopup.onClosePopup()
            }
        })
    },
    toDetail(e){
        let id = e.currentTarget.dataset.id
        wx.navigateTo({
            url:'/pages/detail/index?id='+id
        })
    },
    toPublishDetail(){
        if(!this.data.isLogin){
            this.loginPop.onHandlePopup()
            return
        }
        //——————————————————————
        wx.navigateTo({
            url:"/pages/publishDetail/index"
        })
    },
    toReceiveGift(e){
        let gift = e.detail
        wx.navigateTo({
            url:'/pages/receiveGift/index?giftInfo='+JSON.stringify(gift)
        })
    },
    callbackLogin(){
        this.setData({isLogin:true})
    },
    getRecommendList(){
        axios.post('/api/index/index').then(res=>{
            if(res.code==1){
                res.data.recommend.map(v=>{
                    v['images'] = v.images.split(',')
                })
                this.setData({recommendList:res.data.recommend})
            }
            console.log(this.data.recommendList)
        })   
    },
    getActivities(){
        if(this.data.finished) return
        axios.post('/api/index/flag',{
            page:this.data.pages
        }).then(res=>{
            if(res.code==1){
                if(res.data.length>0){
                    this.data.pages+=1
                    if(res.data.length<10){
                        this.setData({
                            finished:true
                        })
                    }
                    res.data.map(v=>{
                        this.data.activities.push(v)
                    })
                    this.setData({
                        activities:this.data.activities
                    })
                }else{
                    this.setData({
                        finished:true
                    })
                }
            }
        })
    }
})
