const axios = require("../../utils/request")
Page({
    data: {
        mainImage:'',
        swipers:[],
        isShowRewardInput:true,
        
        rewardList:[],
        rewardInputType:1, //1=实物奖品;2=现金红包;3=虚拟奖品
        rewardReceiveMethod:2, //1=添加微信领取；2收货地址领取
        contactInfo:{
            id:''
        },
        rewardInputName:'',
        rewardInputNumber:10,
        rewardRedbagNumber:0.5,
        providerInfo:{
            id:''
        },

        introduction:'',

        shareTimes:2,

        auth_code:'',
        replyKeyword:'', //公众号回复关键字
        grantedPublic:{
            id:''
        }
    },
    onLoad(e) {
        if(e.auth_code){
            wx.showLoading({
                title:'验证授权码...',
                icon:'none'
            })
            axios.post('/api/authorize/verification',{
                code:e.auth_code
            }).then(res=>{
                wx.hideLoading()
                if(res.code==1){
                    this.setData({
                        auth_code:e.auth_code
                    })
                }else{
                    wx.showToast({
                        title:res.msg,
                        icon:'none',
                        duration:3000
                    })
                }
            })
            
        }
    },
    onAddMainImage(){
        wx.chooseImage({
            count:1,
            success:(res)=>{
                if(res.errMsg == 'chooseImage:ok'){
                    wx.navigateTo({
                        url:'/pages/cutImage/cropper-example?callbackType=mainImage&image='+res.tempFilePaths[0]
                    })
                }
                
            }
        })
    },
    onAddImage(){
        wx.chooseImage({
            count:1,
            success:(res)=>{
                if(res.errMsg == 'chooseImage:ok'){
                    wx.navigateTo({
                        url:'/pages/cutImage/cropper-example?callbackType=swiperImage&image='+res.tempFilePaths[0]+'&aspectRatio=2'
                    })
                }
            }
        })
    },
    onRemoveSwiper(e){
        let key = e.currentTarget.dataset.k
        if(typeof key == 'number'){
            this.data.swipers.splice(key,1)
            setTimeout(()=>{
                this.setData({swipers:this.data.swipers})    
            },200)
            
        }
    },
    onHandleRewardInput(){
        setTimeout(()=>{
            this.setData({isShowRewardInput:!this.data.isShowRewardInput})
        },200)
    },
    onAddReward(){
        if(this.data.rewardList.length>1){
            wx.showToast({
                icon:'none',
                title:'最多只能添加一个奖品'
            })
            return
        }
        if(this.data.rewardInputName=='' && this.data.rewardInputType!=2){
            wx.showToast({
                icon:'none',
                title:'请填写奖品名称'
            })
            return
        }
        if(this.data.rewardInputNumber<=0 || this.data.rewardInputNumber==''){
            wx.showToast({
                icon:'none',
                title:'奖品数量必须大于1份'
            })
            return
        }
        if(this.data.rewardRedbagNumber<0.01 && this.data.rewardInputType==2){
            wx.showToast({
                icon:'none',
                title:'单个红包金额必须大于或等于0.01元'
            })
            return
        }
        if(this.data.rewardRedbagNumber*this.data.rewardInputNumber<0.3 && this.data.rewardInputType==2){
            wx.showToast({
                icon:'none',
                title:'红包总额不可少于0.3元'
            })
            return
        }
        if(this.data.rewardReceiveMethod==1){
            if(!this.data.contactInfo.type){
                wx.showToast({
                    icon:'none',
                    title:'请设置领奖联系方式'
                })
                return
            }
        }
        wx.showToast({
            icon:'none',
            title:'添加成功'
        })
        setTimeout(()=>{
            this.data.rewardList.push({
                name:this.data.rewardInputType==2?'现金红包':this.data.rewardInputName,
                number:this.data.rewardInputNumber,
                receiveMethod:this.data.receiveMethod,
                type:this.data.rewardInputType,
                RedbagNumber:this.data.rewardRedbagNumber
            })
            this.setData({rewardList:this.data.rewardList})
            this.setData({rewardInputName:''})
        },200)
    },
    onSelectType(){
        wx.showActionSheet({
            itemList: ['实物奖品', '现金红包', '虚拟奖品'],
            success:(res)=>{
                if(res.errMsg = 'showActionSheet:ok' && res.tapIndex>=0 && res.tapIndex<=2){
                    this.setData({rewardInputType:res.tapIndex+1})
                    if(this.data.rewardInputType==3){
                        this.setData({rewardReceiveMethod:1})
                    }
                    if(this.data.rewardInputType==2){
                        this.setData({rewardReceiveMethod:0})
                    }
                    if(this.data.rewardInputType==1){
                        this.setData({rewardReceiveMethod:2})
                    }
                }
            },
        })
    },
    onSelectReceiveMethod(){
        if(this.data.rewardInputType==3){
            wx.showToast({
                title:'虚拟奖品目前只支持私信领取',
                icon:'none',
                duration:2000
            })
            return
        }
        wx.showActionSheet({
            itemList: ['让中奖者联系我', '快递邮寄',],
            success:(res)=>{
                if(res.errMsg = 'showActionSheet:ok' && res.tapIndex>=0 && res.tapIndex<=2){
                    this.setData({rewardReceiveMethod:res.tapIndex+1})
                    if(this.data.rewardReceiveMethod==1){
                        if(!this.data.contactInfo.type){
                            this.onSelectContact()
                        }
                    }
                }
            },
        })
    },
    onSelectContact(){
        wx.navigateTo({
            url:'/pages/contact/index'
        })
    },
    onDeleteReward(){
        this.setData({rewardList:[]})
    },
    onUploadLogo(){
        wx.chooseImage({
            count:1,
            success:(res)=>{
                if(res.errMsg == 'chooseImage:ok'){
                    this.setData({
                        providerLogo:res.tempFilePaths[0]
                    })
                }
            }
        })
    },
    onSelectProvider(e){
        setTimeout(()=>{
            let key = e.currentTarget.dataset.key
            this.setData({providerIndex:key})
        },200)
    },
    onReselectProvider(){
        setTimeout(()=>{
            this.setData({providerInfo:{}})
        },200)
    },
    onInputEditor(e){
        this.setData({
            introduction:e.detail.html
        })
    },
    onSubmit(){

        if(this.data.mainImage == ''){
            wx.showToast({
                title:'请上传产品/活动主图',
                icon:'none'
            })
            return
        }
        
        if(this.data.swipers.length==0 && this.data.auth_code==''){
            wx.showToast({
                title:'请上传至少一张轮播图',
                icon:'none'
            })
            return
        }

        if(this.data.rewardList==0){
            wx.showToast({
                title:'请添加奖品',
                icon:'none'
            })
            return
        }

        if(this.data.shareTimes<=0 && this.data.auth_code==''){
            wx.showToast({
                title:'裂变次数至少为1次',
                icon:'none'
            })
            return
        }
        if(this.data.auth_code && !this.data.grantedPublic.id){
            wx.showToast({
                title:'请授权引流公众号后发布',
                icon:'none'
            })
            return
        }
        if(this.data.auth_code && !this.data.replyKeyword){
            wx.showToast({
                title:'请设置回复关键字',
                icon:'none'
            })
            return
        }
        wx.showLoading({
            title:'发起中...',
            mask:true
        })
        axios.post('/api/activities/add',{
            main_img:this.data.mainImage,
            images:this.data.swipers.join(','),
            type:this.data.rewardInputType,
            send_type:this.data.rewardReceiveMethod,
            name:this.data.rewardList[0].name,
            stock:this.data.rewardList[0].number,
            single_num:this.data.rewardList[0].RedbagNumber,
            introduction:this.data.introduction,
            contact_id:this.data.contactInfo.id,
            provider_id:this.data.providerInfo.id,
            task_num:this.data.shareTimes,
            code:this.data.auth_code,
            empower_public_id:this.data.grantedPublic.id,
            keyword:this.data.replyKeyword
        }).then(res=>{
            if(this.data.rewardInputType==2){
                wx.requestPayment({
                    timeStamp:res.data.pay.timeStamp,
                    nonceStr:res.data.pay.nonceStr,
                    package:res.data.pay.package,
                    signType:res.data.pay.signType,
                    paySign:res.data.pay.paySign,
                    success:(payback)=>{
                        wx.hideLoading()
                        wx.redirectTo({
                            url:'/pages/detail/index?id='+res.data.id
                        })
                    },
                    fail:(payerr)=>{
                        wx.hideLoading()
                        wx.showToast({
                            title:"支付失败",
                            icon:'none'
                        })
                    }
                })
                return
            }
            if(res.code==1){
                wx.showToast({
                    title:'发起成功！',
                    icon:'none',
                    mask:true,
                    duration:200
                }),
                setTimeout(()=>{
                    if(this.data.auth_code==''){
                        wx.redirectTo({
                            url:'/pages/detail/index?id='+res.data.id
                        })
                    }else{
                        wx.switchTab({
                            url:'/pages/index/index'
                        })
                    }
                },200)
            }else{
                wx.showToast({
                    title:res.msg,
                    icon:'none'
                })
            }
        })

    },
    toSetProvider(){
        wx.navigateTo({
            url:"/pages/setProvider/index"
        })
    },
    toGrantPublic(){
        wx.navigateTo({
            url:"/pages/grantPublic/index"
        })
    },

    callbackDoneCut(e){
        wx.showLoading({
            title:'上传中...',
            mask:true
        })
        if(e.type=='mainImage'){
            axios.upLoadImage(e.image).then(res=>{
                wx.hideLoading()
                if(res.code==1){
                    this.setData({
                        mainImage:res.image
                    })
                }
            })
            
            return
        }

        if(e.type=='swiperImage'){
            axios.upLoadImage(e.image).then(res=>{
                wx.hideLoading()
                if(res.code==1){
                    this.data.swipers.push(res.image)
                    this.setData({
                        swipers:this.data.swipers
                    })
                }
            })
        }
    }
})
