const request = require("../../utils/request.js");
Component({
    properties:{
        lockMask:{
            type:Boolean,
            value:true
        }
    },
    data: {
        userInfo: null,
        iv: '',
        encryptedData: '',
    },
    methods: {
        getUserinfos(e){
            wx.showLoading({
                title:'登陆中...',
                icon:'none'
            })
            wx.getUserProfile({
                desc: '用于完善会员资料',
                success: (res) => {
                    this.setData({
                        userInfo: res.userInfo,
                        iv:res.iv,
                        encryptedData:res.encryptedData
                    })
                    wx.login({
                        success: res => {
                            request.post("/api/user/login_hawk",{
                                code: res.code,
                                userInfo:JSON.stringify({
                                    avatarUrl: this.data.userInfo.avatarUrl,
                                    city: this.data.userInfo.city,
                                    country: this.data.userInfo.country,
                                    gender: this.data.userInfo.gender,
                                    province: this.data.userInfo.province,
                                    nickName:  this.data.userInfo.nickName,
                                })
                            },"POST").then(res => {
                                wx.hideLoading()
                                if(res.code==1) {
                                    wx.setStorageSync("token", res.data.userInfo.token);
                                    wx.setStorageSync("userInfo",res.data.userInfo)
                                    this.wmpopup.onHandlePopup()
                                    wx.showTabBar({})
                                    this.triggerEvent('callback',e)
                                } else {
                                    this.getNonUnionIdUser(res.data.sessionKey);
                                }
                            }).catch(err => {
                                console.log(err);
                            });
                        },
                        fail: err => {
                            console.log(err);
                        }
                    });
                }
            })
        },
        getNonUnionIdUser(sessionKey) {
            let that = this;
            request.post("/pep-call/v1/noUnionIdLogin",{
                encryptedData:that.data.encryptedData,
                iv:that.data.iv,
                sessionKey: sessionKey,
                mmpId: 10007,
                inviteSecret:wx.getStorageSync('inviteSecret')?wx.getStorageSync('inviteSecret'):''
            }).then( res => {
                wx.setStorageSync("token", res.data.token);
                this.onShowLoginPop();
            }).catch(err => {
                console.log(err);
            });
        },
        onHandlePopup(){
            wx.hideTabBar({
                animation:true,
                fail(err){
                    console.log(err)
                }
            })
            this.wmpopup = this.selectComponent('#wm-popup')
            this.wmpopup.onHandlePopup()
        },
        onTapMask(){
            if(!wx.getStorageSync('token')) return
            wx.showTabBar({
                animation:false,
                fail(err){
                    console.log(err)
                }
            })
        }
    },
})

