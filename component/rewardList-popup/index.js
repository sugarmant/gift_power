Component({
    data: {
        loadInfo:[]
    },
    ready(){
        
    },
    methods: {
        onHandlePopup(_data){
            this.setData({
                loadInfo:_data
            })
            this.wmpopup = this.selectComponent('#wm-popup')
            this.wmpopup.onHandlePopup()
        },
        onClose(){
            this.wmpopup = this.selectComponent('#wm-popup')
            this.wmpopup.onHandlePopup()
        }
    }
})

