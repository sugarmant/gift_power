Component({
    properties:{
        lockMask:{
            type:Boolean,
            value:false
        },
        mask:{
            type:Boolean,
            value:true
        },
        width:{
            type:String,
            value:"75%"
        }
    },
    data: {
        isShow:false
    },
    methods: {
        onTapMask(){
            this.triggerEvent('onTapMask')
            if(!this.data.lockMask) this.onHandlePopup()
        },
        onHandlePopup(){
            let bool = this.data.isShow
            if(bool){
                setTimeout(()=>{
                    this.setData({isShow:false})
                },200)
            }else{
                this.setData({isShow:true})
            }
            this.animate("#wm-mask",[
                {opacity:bool?1:0},
                {opacity:bool?0:1}
            ],200)
            this.animate("#wm-popup",[
                {opacity:bool?1:0,top:bool?'50%':'60%'},
                {opacity:bool?0:1,top:bool?'60%':'50%'}
            ],200)
        },
        onClosePopup(){
            setTimeout(()=>{
                this.setData({isShow:false})
            },200)
            this.animate("#wm-mask",[
                {opacity:1},
                {opacity:0}
            ],200)
            this.animate("#wm-popup",[
                {opacity:1,top:'50%'},
                {opacity:0,top:'60%'}
            ],200)
        }
    },
    
})

