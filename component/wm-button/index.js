Component({
    properties:{
        querys:{
            type:String,
            value:""
        },
        params:{
            type:Object,
            value:false
        },
        openType:{
            type:String,
            value:'default'
        },
        sessionFrom:{
            type:String,
            value:''
        },
        sendMessageTitle:{
            type:String,
            value:''
        },
        sendMessagePath:{
            type:String,
            value:''
        },
        sendMessageImg:{
            type:String,
            value:''
        },
        showMessageCard:{
            type:String,
            value:''
        },
        type:{
            type:String,
            value:'default'
        },
        style:{
            type:String,
            value:''
        }
    },
    data: {
        potLocation:[]
    },
    methods: {
        onGetUserinfos(e){
            console.log(e)
            this.triggerEvent('onGetUserinfo',e.detail)
        },
        onContact(e){
            this.triggerEvent('onContact',e.detail)
        },
        onClickButton(e){
            this.returnBack(e)
        },
        onClickButton_simple(e){
            this.returnBack(e)
        },
        onTouchStartButton_simple(e){
            const self = this
            const query = wx.createSelectorQuery().in(this)
            query.select('#content').boundingClientRect(function(res){
                self.setData({
                    potLocation:[e.touches[0].pageX-res.left,e.touches[0].pageY-res.top]
                })
                self.animate("#pot",[
                    {matrix:[0.1, 0, 0,0.1, 0, 0],opacity:1},
                    {matrix:[1, 0, 0,1, 0, 0],opacity:0}
                ],5000)
            }).exec()
        },
        onTouchEndButton_simple(){
            setTimeout(()=>{
                this.clearAnimation('#pot')
                this.animate("#pot",[
                    {matrix:[0.1, 0, 0,0.1, 0, 0],opacity:1},
                    {matrix:[1, 0, 0,1, 0, 0],opacity:0}
                ],300)
            },50)
        },
        returnBack(e){
            console.log(this.data.querys)
            if(this.data.querys!="" && typeof this.data.querys=="string"){
                try{
                    this.data.querys = JSON.parse(this.data.querys)
                    this.triggerEvent('onClickButton',this.data.querys)
                }catch(err){
                    console.error('wm-button参数传递错误：',this.data.querys)
                }
            }else{
                this.triggerEvent('onClickButton',this.data.params)
            }
        }
    },
})

