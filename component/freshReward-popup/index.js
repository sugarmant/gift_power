const request = require("../../utils/request.js");
Component({
    data: {
        isShow:false,
        loadInfo:{
            name:'奖品名称',
            provideName:'提供者名称',
            awardUrl:'https://thirdwx.qlogo.cn/mmopen/vi_32/IkzKmgCxTWCRcB8O4jjiaa5PjoXv49bm66PFUED5EQQtfRmwHDKD76thU1OQyicicO9I7DlUlUouPrA4GtEs7Lmew/132'
        }
    },
    ready(){
        
    },
    methods: {
        beamRotate(){
            this.animate("#beams",[
                {rotateZ:0},
                {rotateZ:360}
            ],7500,()=>{
                this.beamRotate()
            })
        },
        onHandlePopup(_data){
            if(_data) this.setData({loadInfo:_data})
            this.setData({isShow:true})

            this.animate("#gotReward-popup",[
                {opacity:0},
                {opacity:1}
            ],200)
            this.animate("#popup",[
                {scale:[0.2]},
                {scale:[1.3]},
                {scale:[1]}
            ],300)
            
            if(this.data.isShow){
                this.beamRotate()
            }
        },
        onReceiveGift(){
            this.onHandlePopupClose()
            this.triggerEvent('onClickReceiveButton',this.data.loadInfo)
        },
        onHandlePopupClose() {
            this.setData({isShow:false})
        }
    }
})

