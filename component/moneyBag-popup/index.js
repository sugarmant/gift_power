Component({
    data: {
        rewardList:[],
        loadInfo:{
            money:0
        },
    },
    properties:{
        buttonText:{
            type:String,
            value:"收下红包"
        },
        contentText:{
            type:String,
            value:"现金已存入余额账户，前往(我的->红包余额)即可提现"
        }
    },
    ready(){
        
    },
    methods: {
        onHandlePopup(_data){
            this.setData({
                loadInfo:_data
            })
            this.wmpopup = this.selectComponent('#wm-popup')
            this.wmpopup.onHandlePopup()
        },
        onClose(){
            this.wmpopup = this.selectComponent('#wm-popup')
            this.wmpopup.onClosePopup()
        },
        onClickButton(e){
            this.returnBack(e)
        },
        returnBack(e){
            console.log(this.data.querys)
            if(this.data.querys!="" && typeof this.data.querys=="string"){
                try{
                    this.data.querys = JSON.parse(this.data.querys)
                    this.triggerEvent('onClickButton',this.data.querys)
                }catch(err){
                    console.error('wm-button参数传递错误：',this.data.querys)
                }
            }else{
                this.triggerEvent('onClickButton',this.data.params)
            }
        }
    }
})

