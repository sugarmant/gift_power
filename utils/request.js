// const baseUrl = 'http://www.reward.zly'  // 赵林勇本地地址
const baseUrl = 'https://pw.yyncm.com'  // 线上测试地址

const playboyUrl = 'https://test.playboy.shanghuitui.com';  //仓库测试地址

class axios{
    constructor(){
        
    }
    
    put(_url,_data){
        return new Promise((callBack,errorBack)=>{
            this.request('PUT',_url,_data).then(res=>{
                callBack(res)
            }).catch(err=>{
                errorBack(err)
            })
        })
    }
    post(_url,_data){
        return new Promise((callBack,errorBack)=>{
            this.request('POST',_url,_data).then(res=>{
                callBack(res)
            }).catch(err=>{
                errorBack(err)
            })
        })
    }
    get(_url,_data){
        return new Promise((callBack,errorBack)=>{
            this.request('GET',_url,_data).then(res=>{
                callBack(res)
            }).catch(err=>{
                errorBack(err)
            })
        })
    }
    upLoadImage(_filePath,_data){
        return new Promise((callBack,errorBack)=>{
            wx.uploadFile({
                url:baseUrl+"/api/common/upload",
                filePath:_filePath,
                name:'file',
                header:{
                    'token': wx.getStorageSync('token')
                },
                formData:_data,
                success:res=>{
                    let data = JSON.parse(res.data)
                    if(data.code==1){
                        callBack({
                            code:1,
                            image:data.data.fullurl
                        })
                    }
                    
                }
            })
        })
        
    }
    request(_method,_url,_data){
        return new Promise((callBack,errorBack)=>{
            wx.request({
                url:baseUrl+_url,
                data:_data,
                method:_method,
                header:{
                    'Content-type': _method === "GET"?'x-www-form-urlencoded':'application/x-www-form-urlencoded',
                    'token': wx.getStorageSync('token')
                },
                success:res=>{
                    console.log(res)
                    if(res.statusCode==401){
                        wx.removeStorageSync('token');
                        console.log('run')
                        wx.switchTab({
                            url:"/pages/index/index",
                            success:()=>{
                                wx.showToast({
                                    icon:"none",
                                    title:"Token验证失效，请重新登录尝试",
                                    duration:3000
                                })
                            }
                        })
                        return
                    }
                    if(res.statusCode!=200 || res.data.code!=1){
                        res['requestUrl'] = _url
                        res['method'] = _method
                        res['requestData'] = _data
                        if(res.statusCode!=200){
                            wx.showToast({
                                title:'系统繁忙，请稍后再试',
                                icon:'none',
                                duration:3000
                            })
                            return
                        }
                    }
                    if(res.data.code == 1) {
                        callBack(res.data);
                    }else{
                        callBack(res.data);
                    }
                },
                fail:err=>{
                    errorBack(err.data)
                }
              })
        })
    }
    getStore(_url,_data){
        return new Promise((callBack,errorBack)=>{
            wx.request({
                url:playboyUrl+_url,
                data:_data,
                method:"POST",
                header:{
                    'Content-type':'application/json',
                    'Authorization': wx.getStorageSync('token')
                },
                success:res=>{
                    
                    if(res.data.code == 1) {
                        callBack(res.data);
                    } else if(res.data.code == 401) {
                        wx.removeStorageSync('token');
                    } else if(res.data.code == 500){
                        errorBack(res.data);
                    } else {
                        callBack(res.data);
                    }
                },
                fail:err=>{
                    wx.showToast({
                        title:JSON.stringify(err),
                        icon:'none'
                    })
                    errorBack(err.data)
                }
              })
        })
    }
}

module.exports = new axios()