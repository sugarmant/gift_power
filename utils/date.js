const axios = require('./request')

class date{
    standard(_stamp){
        if((_stamp+'').length==10){
            _stamp+='000'
            _stamp=Number(_stamp)
        }
        console.log(_stamp)
        const stamp = new Date(_stamp)
        const year = stamp.getFullYear()
        const month = stamp.getMonth() + 1
        const day = stamp.getDate()
        const hour = stamp.getHours()
        const minute = stamp.getMinutes()
        const second = stamp.getSeconds()

        let hourStr = ('0'+hour).slice(-2)
        let minuteStr = ('0'+minute).slice(-2)
        if(_stamp==null){
            return ''
        }
        return month+'月'+day+'日'+' '+hourStr+':'+minuteStr
    }
    countMinus(_stamp,_nowTime){
        let new_date;
        if(_nowTime){
            new_date = new Date(_nowTime)
        }else{
            new_date = new Date();
        }
        
        let old_date = new Date(_stamp);
        let difftime = (old_date - new_date)/1000;
        let days = parseInt(difftime/86400);
        let hours = parseInt(difftime/3600)-24*days; 
        let minutes = parseInt(difftime%3600/60);
        let seconds = parseInt(difftime%60);
        
        let hoursStr = ('0'+hours).slice(-2)
        let minutesStr = ('0'+minutes).slice(-2)
        let secondsStr = ('0'+seconds).slice(-2)
        if(difftime<=0){
            return false
        }
        if(days>0){
            return days+'天'
        }
        return hoursStr+':'+minutesStr+':'+secondsStr
    }
    getServerTime(){
        return new Promise((callback,errorback)=>{
            axios.get('/plgj/v1/time').then(res=>{
                callback(res)
            })
        })
    }
}

module.exports = new date()